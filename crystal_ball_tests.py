# asetup AnalysisBase,21.2.161 - needs python2 to read input pickle
import ROOT

f1 = ROOT.TF1("mycb", "crystalball", -10, 10)

f1.SetParameter("Constant", 1)
f1.SetParameter("Mean", 1)
f1.SetParameter("Sigma", 1)
f1.SetParameter("Alpha", 1)
f1.SetParameter("N", 1)
f1.SetLineColor(1)
f1.Draw()
ROOT.gPad.Print("cb.pdf[")

values = [0.5, 2.0]

leg = ROOT.TLegend(0.65, 0.65, 0.9, 0.9)
leg.SetBorderSize(0)
leg.SetFillStyle(0)

# put functions here so they're not deleted because of scope
functions = []

for p in range(f1.GetNpar()):
    print("Parameter %d ('%s') will now be varied" % (p, f1.GetParName(p)))
    f1.Draw() # draws the function with all parameters set to 1
    leg.Clear()
    leg.AddEntry(f1, "Nominal", "l")
    for i,v in enumerate(values):
        ftmp = f1.Clone("p%d_%d" % (p, i))
        print(i, v)
        ftmp.SetParameter(f1.GetParName(p), v)
        ftmp.SetLineColor(i+2)
        ftmp.Draw("SAME")
        functions.append(ftmp)
        leg.AddEntry(ftmp, "%s = %.1f" % (f1.GetParName(p), v), "l")
        #print("Parameter %d ('%s'): %f" % (p, f1.GetParName(p), f1.GetParameter(p)))
    leg.Draw()
    ROOT.gPad.Print("cb.pdf")

# close the pdf
ROOT.gPad.Print("cb.pdf]")
