import ROOT, numpy

# make a toy simulation to generate the measurements and see if the fit can determine the parameters of the beams. Idea:
# 1. Assume each beam is described by a 2D gaussian in the transverse plane, with individual mu_x, mu_y, sigma_x, sigma_y
# 2. Move the beams wrt each other and calculate the integral of the product, this should be correlated to the lumi measured at each separation point.

ROOT.gStyle.SetOptStat(0)

print("Will now do a toy simulation of two beams scanned in x and y")

beam1 = ROOT.TF2("Beam1", "[N1]*TMath::Gaus(x,[mu_x1],[sigma_x1])*TMath::Gaus(y,[mu_y1],[sigma_y1])", -0.1, 0.1, -0.1, 0.1)
beam2 = ROOT.TF2("Beam2", "[N2]*TMath::Gaus(x,[mu_x2],[sigma_x2])*TMath::Gaus(y,[mu_y2],[sigma_y2])", -0.1, 0.1, -0.1, 0.1)
beam1.SetParameter("N1", 2)
beam1.SetParameter("mu_x1", 0); beam1.SetParameter("sigma_x1", 0.02);
beam1.SetParameter("mu_y1", 0); beam1.SetParameter("sigma_y1", 0.02);
beam2.SetParameter("N2", 2)
beam2.SetParameter("mu_x2", 0); beam2.SetParameter("sigma_x2", 0.02);
beam2.SetParameter("mu_y2", 0); beam2.SetParameter("sigma_y2", 0.02);
beam1.SetLineColor(ROOT.kBlue); beam1.SetLineWidth(1)
beam2.SetLineColor(ROOT.kRed); beam2.SetLineWidth(1)
#beam1.SetNpx(50); beam1.SetNpy(50);
#beam2.SetNpx(50); beam2.SetNpy(50);

c = ROOT.TCanvas("Toy simulation", "Toy simulation", 1200, 800)
c.Divide(2,2)
c.cd(1)
pdfName = "ToySimulation"

toyMeasurement = ROOT.TGraph2D()
toyMeasurement.SetTitle("Luminosity;Separation x [mm];Separation y [mm];Luminosity")
#toyMeasurement.SetMinimum(0)

separations = numpy.linspace(-0.04, 0.04, 9)
print(separations)
positions = [(x, 0) for x in separations]
positions.extend([(0, y) for y in separations])
print(positions)

for i, p in enumerate(positions):
    print("Will now do this point: (x, y) = (%g, %g)" % (p[0], p[1]))

    # let's draw the 2D histograms representing the densities of beams 1 and 2 as function of x-y,
    c.cd(1)
    beam1.Draw("SURF")
    beam2.SetParameter("mu_x2", p[0])
    beam2.SetParameter("mu_y2", p[1])
    beam2.Draw("SURF same")
    # and the product of them, representing the location and amount of collisions
    c.cd(2)
    product = ROOT.TF2("product", "Beam1*Beam2", -0.1, 0.1, -0.1, 0.1)
    product.SetLineColor(ROOT.kBlack); product.SetLineWidth(1)
    #product.SetNpx(50); product.SetNpy(50);
    product.Draw("SURF")
    a = numpy.array([-0.075, 0.075])
    print("Max of beam 1: %g" % beam1.GetMaximum(a))
    print("Max of beam 2: %g" % beam2.GetMaximum(a))
    print("Max of product: %g" % product.GetMaximum(a))
    product.GetZaxis().SetRangeUser(0, beam1.GetMaximum(a)*beam2.GetMaximum(a))
    ROOT.gPad.Update()
    lumi = product.Integral(-0.1, 0.1, -0.1, 0.1)
    print("Added lumi point: %g" % lumi)
    toyMeasurement.SetPoint(i, p[0], p[1], lumi)
    #ROOT.gPad.WaitPrimitive()
    
    # now draw the resulting lumi as function of separation in x and y
    c.cd(3)
    # trick to use an empty histogram to get the axis ranges right
    temphist = ROOT.TH2F("temphist", "Luminosity as function of separation;Separation x [mm];Separation y [mm]", 10, -0.05, 0.05, 10, -0.05, 0.05)
    temphist.Draw("LEGO3 0")
    temphist.GetZaxis().SetRangeUser(0, 0.006)
    temphist.GetXaxis().SetTitleOffset(1.8); temphist.GetYaxis().SetTitleOffset(1.8)
    toyMeasurement.SetHistogram(temphist)
    toyMeasurement.Draw("p0")
    # add to output pdf, open and close for first and last, respectively
    ext = ".pdf"
    if i == 0:
        ext += "("
    elif i == len(positions)-1:
        ext += ")"
    #c.Update()
    c.Print(pdfName+ext)
    ROOT.gPad.WaitPrimitive()
