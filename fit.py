# asetup AnalysisBase,21.2.161 - needs python2 to read input pickle
import ROOT, pickle, ctypes

# pickle has a list with one dictionary
dict = pickle.load(open("fits_4WP_integrated_fit_integrated_trk_run_350842.pickle", "rb" ) )[0]
print(dict.keys())

# lumi values in 'exp_lumi_x', errors in 'err_x', x separation in 'sep_x', and same for y

# make a 2D profile for the measurements
m = ROOT.TGraph2DErrors()
m.SetTitle("Luminosity;Separation x [mm];Separation y [mm];Luminosity")

# along x measurements, y is 0 by definition
for p in range(0, len(dict['exp_lumi_x'])):
    m.SetPoint(p, dict['sep_x'][p], 0, dict['exp_lumi_x'][p])
    m.SetPointError(p, 0, 0, dict['err_x'][p])
    print("Added point (x, y, z +- err) = (%g, %g, %g +- %g)" % (dict['sep_x'][p], 0, dict['exp_lumi_x'][p], dict['err_x'][p]))
offset = m.GetN()
for p in range(0, len(dict['exp_lumi_y'])):
    m.SetPoint(p+offset, 0, dict['sep_y'][p], dict['exp_lumi_y'][p])
    m.SetPointError(p+offset, 0, 0, dict['err_y'][p])
    print("Added point (x, y, z +- err) = (%g, %g, %g +- %g)" % (0, dict['sep_y'][p], dict['exp_lumi_y'][p], dict['err_y'][p]))

# draw the histogram first to get the axes, then graph with err(ors) and P(oints)
hist = m.GetHistogram(); hist.SetTitle(m.GetTitle())
hist.Draw("A")
m.GetZaxis().SetRangeUser(0, 70) # adjust z range
m.GetXaxis().SetTitleOffset(1.8); m.GetYaxis().SetTitleOffset(1.8)
m.Draw("err p0")
ROOT.gPad.Update()
ROOT.gPad.WaitPrimitive() # wait

# fit a 2D gaussian
f = ROOT.TF2("Single2DGaussian", "[0]*TMath::Gaus(x,[1],[2])*TMath::Gaus(y,[3],[4])", -0.05, 0.05, -0.05, 0.05)
f.SetParameters(50, 0, 0.02, 0, 0.02)
fr = m.Fit(f, "RS")
print("chi2 = %.3f, ndof = %d, chi2/ndof = %.3f" % (fr.Chi2(), fr.Ndf(), fr.Chi2()/fr.Ndf()))
f.Print()
for par in range(0, f.GetNpar()):
    print("Parameter %d (%s): %g" % (par, f.GetParName(par), f.GetParameter(par)))
ROOT.gPad.WaitPrimitive() # wait
f.SetTitle(m.GetTitle())
f.GetXaxis().SetTitleOffset(1.8); f.GetYaxis().SetTitleOffset(1.8)
f.Draw("SURF")
m.Draw("err p0 same")
ROOT.gPad.Update()
ROOT.gPad.WaitPrimitive() # wait

# fit product of two 2D gaussians - what we measure is the product of two beams which we expect to each be described in the transverse plane by a 2D gaussian
ROOT.Math.MinimizerOptions.SetDefaultMaxFunctionCalls(100000) # seems this maxes out at 2.8k!
f2 = ROOT.TF2("Double2DGaussian", "([N_x1]*TMath::Gaus(x,[mu_x1],[sigma_x1])+[N_x2]*TMath::Gaus(x,[mu_x2],[sigma_x2]))*([N_y1]*TMath::Gaus(y,[mu_y1],[sigma_y1])+[N_y2]*TMath::Gaus(y,[mu_y2],[sigma_y2]))", -0.05, 0.05, -0.05, 0.05)
#f2.SetNpx(100); f2.SetNpy(100);
# set parameters for the various terms
f2.SetParameter("N_x1", 7); f2.SetParameter("mu_x1", 0.001); f2.SetParameter("sigma_x1", 0.01)
f2.SetParameter("N_x2", 1.); f2.SetParameter("mu_x2", 0.001); f2.SetParameter("sigma_x2", 0.01)
f2.SetParameter("N_y1", 7); f2.SetParameter("mu_y1", 0.001); f2.SetParameter("sigma_y1", 0.01)
f2.SetParameter("N_y2", 1.); f2.SetParameter("mu_y2", 0.001); f2.SetParameter("sigma_y2", 0.01)
fr = m.Fit(f2, "RS")
print("chi2 = %.3f, ndof = %d, chi2/ndof = %.3f" % (fr.Chi2(), fr.Ndf(), fr.Chi2()/fr.Ndf()))
f2.Print()
for par in range(0, f2.GetNpar()):
    print("Parameter %d (%s): %g" % (par, f2.GetParName(par), f2.GetParameter(par)))
ROOT.gPad.WaitPrimitive() # wait
f2.SetTitle(m.GetTitle())
f2.GetXaxis().SetTitleOffset(1.8); f2.GetYaxis().SetTitleOffset(1.8)
f2.Draw("SURF")
m.Draw("err p0 same")
ROOT.gPad.Update()
ROOT.gPad.WaitPrimitive() # wait

## draw the deviations in each point
#for p in range(0, m.GetN()):
#    l = ROOT.TPolyLine3D()
#    x, y, z = (ctypes.c_double(),)*3 # initialize vars
#    m.GetPoint(p, x, y, z)
#    l.SetNextPoint(x.value, y.value, z.value)
#    l.SetNextPoint(x.value, y.value, f.Eval(x.value, y.value))
#    print("Point %d: m = %g, f = %g" % (p, z.value, f.Eval(x.value, y.value)))
#    l.SetLineWidth(2); l.SetLineColor(ROOT.kGreen)
#    l.Draw()
