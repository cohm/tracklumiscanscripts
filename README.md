# TrackLumiScanScripts

Simple repo with standalone python scripts for simple studies of van der Meer scans (or emittance scans) related to track-counting luminosity. 

## Instructions

The scripts are meant to be used in the environment of the ATLAS analysis releases and only need python (python2 in order to read the provided .pickle input file), ROOT and numpy.
